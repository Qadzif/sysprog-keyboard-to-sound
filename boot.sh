#!/bin/bash
### BEGIN INIT INFO
#Provides: check
#Required-Start: $all
#Required-Stop:
#Default-Start: 2 3 4 5
#Default-Stop:
#Short-Description: checker...
### END INIT INFO

# Carry out specific functions when asked to by the system
case "$1" in
        start)
                echo "Starting script"
                echo "Check if amixer and aplay available"
                command -v amixer > /dev/null && echo "amixer available" || echo "amixer is unavailable"
                command -v aplay > /dev/null && echo "aplay available" || echo "aplay is unavailable"
                ;;
        stop)
                echo "Stopping script"
                echo "its stop"
                ;;
        *)
                echo "Usage: /root/terminal-mid.sh {start|stop}"
                exit 1
                ;;
esac
exit 0

