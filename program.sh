#!/bin/bash

printf 'KEYBOARD THAT PLAY SOUND\n'
printf 'Press "Q" to exit program\n'
printf 'Press "U" to set volume to 90\n'
printf 'Press "D" to set volume to 60\n'
printf 'Press any other to hear a sound \n'
printf '\n'


while true; do {
	echo "Press a Button : "
	read -rsn1 BUTTON
	if [[ $BUTTON =~ [Uu] ]]
	then
	amixer set Master 90%

	elif [[ $BUTTON =~ [Dd] ]]
	then amixer set Master 60%

	elif [[ $BUTTON =~ [Qq] ]]
	then exit 0

	else
	aplay C4.wav

	fi
}
done
